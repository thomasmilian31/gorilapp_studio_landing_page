import { FirestoreDataConverter } from "firebase/firestore/lite";

export interface Price {
  id: string
  interval: "month" | "year"
  unitAmount: number
}

export const priceConverter: FirestoreDataConverter<Price> = {
  toFirestore(price: Price) {
    return {
      id: price.id,
      billingInterval: price.interval,
      unitAmount: price.unitAmount
    };
  },
  fromFirestore(snapshot): Price {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      interval: data.billingInterval,
      unitAmount: data.unitAmount,
    };
  },
}
