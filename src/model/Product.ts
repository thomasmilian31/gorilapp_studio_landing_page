import { FirestoreDataConverter } from "firebase/firestore/lite";

export interface Product {
  id: string
  name: string
  description: string
  includedFeatures: string[]
  notIncludedFeatures?: string[]
}

export const productConverter: FirestoreDataConverter<Product> = {
  toFirestore(product: Product): Product {
    return {
      id: product.id,
      name: product.name,
      description: product.description,
      includedFeatures: product.includedFeatures,
      notIncludedFeatures: product.notIncludedFeatures
    };
  },
  fromFirestore(snapshot): Product {
    const data = snapshot.data();
    return {
      id: snapshot.id,
      name: data.name,
      description: data.description,
      includedFeatures: data.includedFeatures,
      notIncludedFeatures: data.notIncludedFeatures
    };
  }
}
