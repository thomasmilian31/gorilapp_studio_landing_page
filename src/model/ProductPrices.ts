import { Price } from "./Price";
import { Product } from "./Product";

export interface ProductPrices extends Product {
  prices: Price[]
}
