import { Price, priceConverter } from "@/model/Price";
import { Product, productConverter } from "@/model/Product";
import { ProductPrices } from "@/model/ProductPrices";
import { initializeApp } from "firebase/app";
import { collection, getDocs, getFirestore } from "firebase/firestore/lite";

const firebaseConfig = {
  apiKey: "AIzaSyB_nfUsPznbp9YsDUWfllov7cYra0bi0c4",
  authDomain: "gorilapp.firebaseapp.com",
  projectId: "gorilapp",
  storageBucket: "gorilapp.appspot.com",
  messagingSenderId: "754541334348",
  appId: "1:754541334348:web:ffacdf078bfb22d17889e9",
  measurementId: "G-F3KKSMSN5K"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

async function fetchProducts(): Promise<Product[]> {
  const productsCollection = collection(db, "products").withConverter(productConverter)
  const querySnapshot = await getDocs(productsCollection)
  return querySnapshot.docs.map((doc) => doc.data())
}

async function fetchPrices(productId: string): Promise<Price[]> {
  const pricesCollection = collection(db, "products", productId, "prices").withConverter(priceConverter)
  const querySnapshot = await getDocs(pricesCollection)
  return querySnapshot.docs.map((doc) => doc.data())
}

export async function fetchProductsWithPrices(): Promise<ProductPrices[]> {
  const products = await fetchProducts()
  const productPrices = await Promise.all(products.map(async (product) => {
    const prices = await fetchPrices(product.id)
    return { ...product, prices }
  }))
  return productPrices.sort((a, b) => a.prices[0].unitAmount - b.prices[0].unitAmount)
}
