import TitleText from "@/components/title_text";
import Titles from "@/components/titles";
import Image from "next/image";

export default function Home() {
  return (
    <div className="container mx-auto">
      <h1>Home</h1>

      <Titles 
        chipIcon="/account_tree.svg"
        chipText="Features"
        title1="Features that you'll love"
        subtitle="We've got everything you need to build apps faster and easier with low code." />

      <div className="grid grid-cols-4 max-w-7xl mx-auto mt-20">
        <Feature />
        <Feature />
        <Feature />
        <Feature />
        <Feature isBottom />
        <Feature isBottom />
        <Feature isBottom />
        <Feature isBottom />
      </div>
    </div>
  )

  interface FeatureProps {
    title?: string;
    subtitle?: string;
    image?: string;
    isBottom?: boolean;
  }

  function Feature({ title, subtitle, image, isBottom }: FeatureProps) {
    return (
      <div className={`flex h-[180px] from-[#ffffff1A] border-white border-opacity-10 ${isBottom ? "hover:bg-gradient-to-b" : "hover:bg-gradient-to-t border-b"}`}>
        <div className="w-[1px] relative">
          <div className="absolute top-0 bottom-0 left-0 right-0 bg-white opacity-10" />
          <div className="absolute top-1/2 -translate-y-1/2 left-0 right-0 bg-white opacity-40 h-[16px] m-auto" />
          <div className={`absolute left-0 right-0 bg-white opacity-20 h-[4px] m-auto ${isBottom ? "bottom-0" : "top-0"}`} />
        </div>

        <div className="relative w-full mx-10">
          <Image src={"/account_tree.svg"} alt="icon" height={24} width={24} className="absolute top-1/2 -translate-y-14" />
          <p className="absolute top-1/2 -translate-y-1/2 font-medium text-white">Built for speed</p>
          <p className="absolute top-1/2 translate-y-5 font-light text-white opacity-50 leading-tight">Instantly sync your notes across devices</p>
        </div>
      </div>
    )
  }
}