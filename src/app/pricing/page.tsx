import Titles from "@/components/titles"
import PricingContent from "./pricing-content"
import { fetchProductsWithPrices } from "@/firebase/pricing-datasource"
import Head from "next/head"
import { Metadata } from "next"

export const metadata: Metadata = {
  title: "Goril.app Studio - Low Code Flutter IDE",
  description: "Prices for Goril.app Studio, a low code Flutter IDE that helps you build Flutter apps faster and easier.",
  openGraph: {
    title: "Goril.app Studio - Low Code Flutter IDE",
    description: "Prices for Goril.app Studio, a low code Flutter IDE that helps you build Flutter apps faster and easier.",
    images: "https://goril.app/og-image.png"
  }
}

export default async function Home() {
  const res = await fetchProductsWithPrices()

  return (
    <section className="py-24">
      <Titles
        chipIcon="/local_activity.svg"
        chipText="Pricing"
        title1="Predictable pricing, no surprises"
        subtitle="Start building for free, collaborate with a team, then scale to millions of users" />

      <PricingContent products={res} />
    </section>
  )
}
