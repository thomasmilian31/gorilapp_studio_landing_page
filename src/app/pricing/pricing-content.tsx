'use client'

import ButtonAction from "@/components/button_action";
import Surface from "@/components/surface";
import { ProductPrices } from "@/model/ProductPrices";
import Image from "next/image";
import { useState } from "react";

interface PricingContentProps {
  products: ProductPrices[]
}

export default function PricingContent({ products }: PricingContentProps) {
  const [toggle, setToggle] = useState<'month' | 'year'>('year')

  return (
    <div className="m-4 md:m-0 md:mx-auto max-w-6xl flex flex-col items-center relative">
      <Surface className="mt-14 mb-8">
        <div className="flex gap-1 bg-[#212121] rounded-full border border-[#2E2D2D] p-1">
          <ButtonAction text="Billing Monthly" onClick={() => setToggle("month")} icon="/star_rate.svg" selected={toggle === "month"} />
          <ButtonAction text="Billing Annually" onClick={() => setToggle("year")} icon="/workspace_premium.svg" selected={toggle === "year"} />
        </div>
      </Surface>

      <Surface roundedCustom className="rounded-[42px]">
        <div className="border border-[#2C2D3C] rounded-[32px] bg-[#211F1F] bg-opacity-50">
          <PricingCards interval={toggle} products={products} />
        </div>
      </Surface>

      <Image
        src={"/shadow.png"}
        alt="Vercel Logo"
        width={1400}
        height={680}
        className="absolute -z-10 -top-28" />
    </div>
  )
}

interface PricingCardsProps {
  products: ProductPrices[]
  interval: "month" | "year"
}

export function PricingCards({ products, interval }: PricingCardsProps) {
  return (
    <div className="m-3 sm:m-8 grid gap-4 grid-cols-1 lg:grid-cols-3">
      {products.map((product, index) => {
        return (<PricingCard
          key={index}
          product={product}
          interval={interval}
          highlighted={index === products.length - 1}
        />)
      })}
    </div>
  )
}

interface PricingCardProps {
  product: ProductPrices
  interval: "month" | "year"
  highlighted?: boolean
}

function PricingCard({ product, interval, highlighted }: PricingCardProps) {
  let price = product.prices.find(price => price.interval === interval)
  if (!price) {
    price = product.prices[0]
  }

  let amount = price.unitAmount / 100
  if (interval === "year") {
    amount = amount / 12
    amount = Math.round(amount)
  }

  return (
    <div className="relative text-white">
      {/* Highlighted background */}
      {highlighted &&
        <div className="absolute inset-0 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1]" />
      }

      {/* Card */}
      <div className={"relative h-full max-w-md rounded-2xl p-8 sm:p-10 flex flex-col bg-[#211F1F] border border-[#333030] " + (highlighted ? "card-pricing-highlighted" : "")}>

        {/* Title */}
        <p className="bg-gradient-to-br from-[#A098F3] to-[#574BD2] bg-clip-text text-transparent font-display text-2xl font-bold">{product.name}</p>
        {/* Description */}
        <p className="mt-4 text-sm">{product.description}</p>

        {/* -------------- */}
        <hr className="border-[#333030] my-6" />

        {/* Price */}
        <p className="font-semibold text-[40px] mb-1">€{amount}</p>
        <p className="text-[#A5B0D9] text-[10px]">per month</p>

        {/* -------------- */}
        <hr className="border-[#333030] my-6" />

        {/* Features */}
        {/* <p className="font-semibold text-sm mb-4">Features title</p> */}
        <div className="grid gap-4">
          {product.includedFeatures.map((feature, index) => {
            return (<FeatureLine
              key={index}
              title={feature}
            />)
          })}
          {product.notIncludedFeatures && product.notIncludedFeatures.map((feature, index) => {
            return (<FeatureLine
              key={index}
              title={feature}
              isIncluded={false}
            />)
          })}
        </div>
      </div>
    </div>
  )
}

function FeatureLine({ title, isIncluded = true }: { title: string, isIncluded?: boolean }) {
  return (
    <div className="flex">
      <Image src={isIncluded ? "/check_circle.svg" : "/block.svg"} alt="icon" height={20} width={20} />
      <p className="text-sm font-normal ml-2">{title}</p>
    </div>
  )
}
