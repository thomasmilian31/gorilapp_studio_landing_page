import Surface from "@/components/surface";
import TitleText from "@/components/title_text";
import { Metadata } from "next";
import Head from "next/head";
import Image from "next/image";

export const metadata: Metadata = {
  title: "Goril.app Studio - Low Code Flutter IDE",
  description: "Donwlod Goril.app Studio, a low code Flutter IDE that helps you build Flutter apps faster and easier.",
  openGraph: {
    title: "Goril.app Studio - Low Code Flutter IDE",
    description: "Donwlod Goril.app Studio, a low code Flutter IDE that helps you build Flutter apps faster and easier.",
    images: "https://goril.app/og-image.png"
  }
}

export default function Home() {
  return (
    <div className="">
      <Head>
        <title>Goril.app Studio - Low Code Flutter IDE</title>
        <meta name="description" content="Donwlod Goril.app Studio, a low code Flutter IDE that helps you build Flutter apps faster and easier." />
      </Head>
      
      <section className="flex flex-col items-center h-screen">

        <div className="my-auto flex flex-col items-center">
          {/* Image */}
          <Image
            src="/light_logo.png"
            alt="Goril.app Studio Logo"
            width={140}
            height={140}
          />

          {/* Title */}
          <h1 className="text-center font-semibold text-3xl md:text-4xl mt-10">
            <TitleText text="Download Studio" />
          </h1>

          <h2 className="text-center leading-normal text-[#D4C7C7] text-lg md:text-xl max-w-2xl mx-4 mt-5">
            Experience the Power of Low Code Development – Get Started for Free!
          </h2>

          {/* Download Buttons */}
          <div className="relative mt-20 flex flex-col gap-6">
            <DownloadButton
              title="macOS"
              subtitle="Downioad for macOS Intel and Silicon (M1/M2) processor."
              icon="/apple.svg"
              link="https://storage.googleapis.com/gorilapp_assets/gorilapp.dmg?ignoreCache=1"
            />

            <div className="absolute m-auto inset-y-0 inset-x-0 lg:inset-x-32 blur-3xl rounded-full bg-gradient-to-b from-[#d2ab50a0] via-[#9670B79E] to-[#7266EDA1]" />
          </div>
        </div>

      </section>
    </div>
  )

  interface DownloadButtonProps {
    title: string;
    subtitle: string;
    icon: string;
    link: string;
  }

  function DownloadButton({ title, subtitle, icon, link }: DownloadButtonProps) {
    return (
      <Surface roundedCustom className="rounded-3xl mx-4">
        <a href={link} className="max-w-lg flex bg-[#211F1F] p-5 rounded-2xl items-center relative z-10 bg-opacity-40 active:scale-95 transition">
          <div className="rounded-lg border-4 border-[#33363c] p-3 aspect-square">
            {/* Apple logo */}
            <Image
              src={icon}
              alt={title}
              width={24}
              height={24}
            />
          </div>

          <div className="ml-4 flex flex-col gap-0.5 grow">
            <div className="text-white text-lg font-semibold">
              {title}
            </div>
            <div className="text-stone-400 text-sm">
              {subtitle}
            </div>
          </div>

          <div>
            <Image
              src="/download.svg"
              alt="Download icon"
              width={24}
              height={24}
            />
          </div>
        </a>
      </Surface>
    )
  }
}