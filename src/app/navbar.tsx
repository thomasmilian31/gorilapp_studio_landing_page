import Image from "next/image";
import Link from "next/link";

export default function NavBar() {
  const menus = [
    { name: "Pricing", route: "/pricing" },
    { name: "Documentation", route: "https://docs.goril.app/" },
    { name: "Download", route: "/download" },
    { name: "Agency", route: "https://agency.goril.app" },
  ]

  return (
    <div className="fixed z-50 navbar backdrop-blur-xl">
      <div className="navbar-start">
        {/* Dropdown */}
        <div className="dropdown">
          {/* Icon dropdown */}
          <label tabIndex={0} className="btn btn-ghost lg:hidden">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
          </label>

          {/* Menu dropdown */}
          <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52">
            {menus.map((menu) => (
              <li key={menu.name}>
                <Link href={menu.route}>{menu.name}</Link>
              </li>
            ))}
          </ul>
        </div>

        {/* Logo */}
        <Link href="/" className="btn btn-ghost normal-case text-xl">
          <Image src="/goril-app.svg" alt="Goril.app" height={24} width={24} className="mr-2 opacity-75" />
          Goril.app
        </Link>
      </div>

      {/* Routes */}
      <div className="navbar-center hidden lg:flex">
        <ul className="menu menu-horizontal px-1">
          {menus.map((menu) => (
            <li key={menu.name}>
              <Link href={menu.route}>{menu.name}</Link>
            </li>
          ))}
        </ul>
      </div>

      {/* CTA */}
      <div className="navbar-end">
        <Link href="/download" className="bg-gradient-to-r from-indigo-400 to-indigo-600 rounded-full text-white px-6 py-2 normal-case opacity-90 active:scale-95 transition ease-in-out">
          Start now
        </Link>
      </div>
    </div>
  )
}
