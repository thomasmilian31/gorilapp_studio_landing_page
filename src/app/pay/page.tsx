'use client'

import { initializePaddle, Paddle } from "@paddle/paddle-js";
import { useEffect, useState } from "react";
import { useSearchParams } from 'next/navigation';
import Image from "next/image";

export default function Home() {
  const [paddle, setPaddle] = useState<Paddle>();

  const searchParams = useSearchParams()

  // Download and initialize Paddle instance from CDN
  useEffect(() => {
    initializePaddle({ environment: 'production', token: 'live_03d3050043aa7f04018fef2dace' }).then(
      (paddleInstance: Paddle | undefined) => {
        if (paddleInstance) {
          setPaddle(paddleInstance);
        }
      },
    );
  }, []);
  
  const transactionId = searchParams.get('_ptxn');
  if (transactionId != undefined) {
    paddle?.Checkout.open({
      settings: {
        theme: 'dark',
        locale: 'en',
        successUrl: 'https://europe-west1-gorilapp.cloudfunctions.net/redirect?url=stripe-success',
        allowLogout: false,
        frameStyle: "width: 100%; min-width: 312px; background-color: transparent; border: none;",
      },
      transactionId: transactionId,
    });
  }

  return (
    <div className="h-full min-h-screen">
      <Image src={"/pay_bg.svg"} alt="icon" layout="fill" objectFit="cover"/>
    </div>
  )
}
