import SectionLowCode from "./sections/section-low-code"
import SectionHeader from "./sections/section-header"
import SectionIntro from "./sections/section-intro"
import SectionIDE from "./sections/section-ide"
import SectionIntegration from "./sections/section-integration"
import SectionOutro from "./sections/section-outro"
import Head from "next/head"
import { Metadata } from "next"

export const metadata: Metadata = {
  title: "Goril.app Studio - Low Code Flutter IDE",
  description: "Goril.app Studio is a low code Flutter IDE that helps you build Flutter apps faster and easier.",
  openGraph: {
    title: "Goril.app Studio - Low Code Flutter IDE",
    description: "Goril.app Studio is a low code Flutter IDE that helps you build Flutter apps faster and easier.",
    images: "https://goril.app/og-image.png"
  }
}

export default function Home() {
  return (
    <div className="flex flex-col gap-40 py-32">
      {/* Header */}
      <SectionHeader />
      
      {/* Intro */}
      <SectionIntro />

      {/* Low code */}
      <SectionLowCode />

      {/* IDE */}
      <SectionIDE />

      {/* Integration */}
      <SectionIntegration />

      {/* Outro */}
      <SectionOutro />
    </div>
  )
}
