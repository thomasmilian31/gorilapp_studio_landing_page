'use client'

import ButtonAction from "@/components/button_action"
import Surface from "@/components/surface"
import Titles from "@/components/titles"
import Image from "next/image"
import { useState } from "react"

export default function SectionLowCode() {
  const [video, setVideo] = useState<'ui' | 'logic' | 'state' | 'custom'>('ui')

  return (
    <section className="flex flex-col items-center relative">
      {/* Titles */}
      <Titles
        chipIcon="/draw.svg"
        chipText="Low Code"
        title1="Unlock the Power of"
        title2="Low Code Development"
        subtitle="Studio is a powerful integrated development environment designed to empower developers with unparalleled creative freedom" />

      <Surface roundedCustom className="mt-14 mb-8 z-10 rounded-[38px]">
        <div className="flex flex-col sm:flex-row gap-1 bg-[#212121] rounded-[24px] border border-[#2E2D2D] p-1">
          <ButtonAction text="UI Builder" icon="/brush.svg" onClick={() => setVideo("ui")} selected={video === "ui"} />
          <ButtonAction text="Actions & Logic" icon="/arrow_split.svg" onClick={() => setVideo("logic")} selected={video === "logic"} />
          <ButtonAction text="State Management" icon="/account_tree.svg" onClick={() => setVideo("state")} selected={video === "state"} />
          <ButtonAction text="Customization" icon="/imagesearch_roller.svg" onClick={() => setVideo("custom")} selected={video === "custom"} />
        </div>
      </Surface>

      <Surface roundedCustom className="rounded-[38px] z-10 sm:mx-6 xl:mx-0 lg:w-[800px] xl:w-[1100px]">
        <div className="border border-[#2C2D3C] rounded-[32px] aspect-video bg-[#211F1F] overflow-hidden">
          <video id="video" autoPlay={true} muted={true} loop={true} className="h-full w-full object-cover" src={`/video-${video}.mp4`} />
        </div>
      </Surface>

      <Image
        src={"/shadow.png"}
        alt="Vercel Logo"
        width={1400}
        height={680}
        className="absolute bottom-0" />
    </section>
  )
}