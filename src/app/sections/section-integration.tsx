import Card from "@/components/card";
import Titles from "@/components/titles";

export default function SectionIntegration() {
  return (
    <section className="flex flex-col items-center">
      {/* Titles */}
      <Titles
        chipIcon="/api.svg"
        chipText="API & Firebase integration"
        title1="Seamlessly Integrate"
        title2="with Your Backend"
        subtitle="Connect to external APIs or leverage the power of Firebase for real-time data synchronization, enhancing your app's functionality."
        className="mb-16" />

      {/* Cards */}
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 max-w-7xl px-8">
        <Card
          title="Connect with Your Backend"
          subtitle="Integrate your app with your backend or any API effortlessly, Goril.app Studio streamlines all the process."
          image="/api.png"
        />
        <Card
          title="Power of Firebase Integration"
          subtitle="Leverage Firebase's robust suite of services, including Authentication, Firestore, Storage, and more. "
          image="/firebase.png"
        />
        <Card
          title="Login Workflow and Role Access"
          subtitle="Easily manage the login workflow within your app. You can also set role-based access to your app's pages and components."
          image="/security.png"
        />
      </div>
    </section>
  )
}