import Card from "@/components/card";
import Titles from "@/components/titles";

export default function SectionIntro() {
  return (
    <section className="flex flex-col items-center">
      {/* Titles */}
      <Titles
        chipIcon="/bolt.svg"
        chipText="Fast development"
        title1="Build Flutter apps faster and"
        title2="easier with low code" />

      {/* Cards */}
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 max-w-7xl px-8">
        <Card
          title="Visual Editor"
          subtitle="Visually design your app's user interface with our intuitive drag-and-drop widget builder."
          image="/visual.png"
          imagePosition="top"
        />
        <Card
          title="State Management"
          subtitle="Simplify state management across components, pages, and more."
          image="/state2.png"
        />
        <Card
          title="Custom Code"
          subtitle="Add custom code effortlessly for limitless possibilities and advanced functionality."
          image="/custom_code.png"
          imagePosition="top left"
        />
        <Card
          title="Debugging"
          subtitle="Debug seamlessly with the project's source code available on your local device."
          image="/debugging2.png"
        />
        <Card
          title="API Integration"
          subtitle="Integrate your own APIs or add Firebase for powerful backend capabilities."
          image="/integration2.png"
        />
        <Card
          title="Pub.dev libraries"
          subtitle="Leverage the power of the Flutter ecosystem with thousands of open-source packages."
          image="/pub-dev3.png"
        />
      </div>
    </section>
  )
}
