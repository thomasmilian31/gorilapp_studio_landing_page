import Surface from "@/components/surface";
import TitleText from "@/components/title_text";
import Image from "next/image";
import Link from "next/link";

export default function SectionOutro() {
  return (
    <section className="flex flex-col items-center">

      {/* Logo */}
      <div className="mx-auto bg-[#7266ED08] w-[280px] h-[280px] p-5 rounded-full relative">
        <div className="bg-[#7266ED1A] w-full h-full p-5 rounded-full">
          <div className="bg-[#7266ED40] w-full h-full p-5 rounded-full">
            <div className="bg-[#7266ED66] w-full h-full p-5 rounded-full flex items-center justify-center">
              <Image src={'/goril-app.svg'} alt="Logo" height={60} width={60} />
            </div>
          </div>
        </div>

        <div className="absolute m-auto inset-0 lg:-inset-40 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1] opacity-100 lg:opacity-50" />
      </div>

      <h2 className="font-semibold text-4xl md:text-5xl text-center mt-4 mb-8">
        <TitleText text="Let's build" />
        <TitleText text="your app together" />
      </h2>

      <Link href="/download">
        <Surface>
          <button className="px-8 py-3 bg-gradient-to-r from-indigo-400 to-indigo-600 rounded-full border border-indigo-300 opacity-90 hover:opacity-100 transition duration-200 ease-in-out hover:border-indigo-500 active:scale-95">
            <div className="text-center text-white text-lg font-medium">Download now</div>
          </button>
        </Surface>
      </Link>

      {/* Studio image */}
      <Image
        src="/header_image.png"
        alt="Vercel Logo"
        className="lg:-mt-52"
        width={1500}
        height={677}
        draggable={false}
        priority
      />
    </section>
  )
}
