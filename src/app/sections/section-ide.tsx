import Card from "@/components/card";
import Surface from "@/components/surface";
import Titles from "@/components/titles";
import Image from "next/image";

export default function SectionIDE() {
  return (
    <section className="flex flex-col items-center relative">
      <Surface roundedCustom className="rounded-[42px] z-10 sm:mx-6 xl:mx-0 lg:w-[800px] xl:w-[1100px]">
        <div className="border border-[#2C2D3C] rounded-[32px] bg-[#211F1F] bg-opacity-90">
          {/* MacOS window control */}
          <div className="m-6 flex gap-2.5">
            <div className="rounded-full h-3 w-3 bg-[#FA5F58]" />
            <div className="rounded-full h-3 w-3 bg-[#FEBC2E]" />
            <div className="rounded-full h-3 w-3 bg-[#28C840]" />
          </div>

          {/* Titles */}
          <Titles
            chipIcon="/data_object.svg"
            chipText="IDE with Custom Code"
            title1="Unleash Your Creativity"
            subtitle="Goril.app Studio's powerful integrated development environment (IDE), designed to empower developers with unparalleled creative freedom."
            className="my-16" />

          <div className="grid grid-cols-1 md:grid-cols-2 gap-12 mx-4 md:mx-12 mb-4 md:mb-12">
            <Card
              title="Custom Code Editor"
              subtitle="Dive into the heart of your app and unleash your coding skills with our full-fledged text editor."
              image="/editor.png"
              border={false}
            />
            <Card
              title="Create Custom Functions and Widgets"
              subtitle="Take your app to new heights by creating your own functions and widgets using your preferred programming language."
              image="/custom_code.png"
              border={false}
            />
            <Card
              title="Extensibility and Modularity"
              subtitle="Effortlessly manage and organize your code, making it easier to maintain and collaborate with your team."
              image="/modularity2.png"
              border={false}
            />
            <Card
              title="Seamless Integration"
              subtitle="With our IDE, you can smoothly integrate your custom code with the visual components and logic created using the low code capabilities."
              image="/pub-dev.png"
              border={false}
            />
          </div>
        </div>
      </Surface>

      <Image
        src={"/shadow.png"}
        alt="Vercel Logo"
        width={1400}
        height={680}
        className="absolute rotate-180 -top-80" />
    </section>
  )
}