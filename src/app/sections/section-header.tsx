import Surface from "@/components/surface";
import Titles from "@/components/titles";
import Image from "next/image";
import Link from "next/link";

export default function SectionHeader() {
  return (
    <main className="flex flex-col items-center">
      {/* Titles */}
      <Titles
        chipIcon="/data_object.svg"
        chipText="Flutter × Low code"
        title1="Supercharge your Flutter"
        title2="app development"
        subtitle="Unleash your creativity and develop Flutter apps effortlessly with our low code Studio designed for rapid development."
        isH1 />

      {/* Button */}
      <div className="mt-14 z-30">
        <Link href="/download">
          <Surface>
            <button className="px-8 py-3 bg-gradient-to-r from-indigo-400 to-indigo-600 rounded-full border border-indigo-300 opacity-90 hover:opacity-100 transition duration-200 ease-in-out hover:border-indigo-500 active:scale-95">
              <div className="text-center text-white text-lg font-medium">Download now</div>
            </button>
          </Surface>
        </Link>
      </div>

      {/* Studio image */}
      <Image
        src="/header_image.png"
        alt="Vercel Logo"
        className="lg:-mt-52"
        width={1500}
        height={677}
        draggable={false}
        priority
      />
    </main>
  )
}