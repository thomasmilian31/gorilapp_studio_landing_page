interface SurfaceProps {
  children: React.ReactNode;
  roundedCustom?: boolean;
  className?: string;
}

export default function Surface({ children, roundedCustom, className }: SurfaceProps) {
  return (
    <div className={`bg-[#141414] bg-opacity-40 p-2.5 border border-[#141414] border-opacity-40 ${roundedCustom ? "" : "rounded-full"} ${className}`}>
      {children}
    </div>
  )
}
