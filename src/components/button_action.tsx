import Image from "next/image";

interface ButtonActionProps {
  text: string;
  icon: string;
  onClick?: () => void;
  selected?: boolean;
  className?: string;
}

export default function ButtonAction({ text, icon, onClick, selected, className }: ButtonActionProps) {
  return (
    <button onClick={onClick} className={`flex gap-2.5 py-2.5 px-3.5 text-[#E3E3E3] items-center rounded-full border border-transparent transition-colors ${selected ? "bg-gradient-to-b from-[#343333] from-65% to-[#2B2828] border-[#3C3A3A]" : "hover:bg-[#2B2828]"} ${className}`}>
      <Image
        src={icon}
        alt="Vercel Logo"
        width={20}
        height={20}
        draggable={false} />
      {text}
    </button>
  )
}