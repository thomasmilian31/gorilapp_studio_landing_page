interface TitleTextProps {
  text: string;
  className?: string;
}

export default function TitleText({ text, className }: TitleTextProps) {
  return (
    <span className={`text-transparent bg-clip-text bg-gradient-to-b from-[#D9D9D9] from-50% to-[#8E8B8B] to-80% py-1 block ${className}`}>
      {text}
    </span>
  )
}