import Image from "next/image";

interface CardProps {
  title: string;
  subtitle: string;
  image: string;
  imagePosition?: "top" | "bottom" | "top left";
  border?: boolean;
  className?: string;
}

export default function Card({ title, subtitle, image, imagePosition, border = true, className }: CardProps) {
  return (
    <div className={`rounded-2xl ${border ? "bg-[#211F1F] border" : "bg-[#1d1b1b]"} border-[#333030] ${className}`}>
      {/* Image */}
      <div className="h-44 relative m-4 rounded-lg overflow-hidden border border-[#3C3A3A] border-opacity-60 mix-blend-lighten">
        <Image
          src={image}
          alt=""
          fill={true}
          style={{ objectFit: 'cover', objectPosition: imagePosition }}
        />

        <div className="absolute m-auto inset-y-12 inset-x-10 blur-3xl opacity-80 rounded-full bg-gradient-to-b from-[#d2ab50a0] via-[#9670B79E] to-[#7266EDA1]" />
      </div>

      {/* Texts */}
      <div className="flex flex-col gap-2 mx-8 mb-8">
        {/* Title */}
        <p className="text-white text-lg font-medium">
          {title}
        </p>

        {/* Subtitle */}
        <p className="text-[#d3d2d2] text-base font-normal leading-relaxed">
          {subtitle}
        </p>
      </div>
    </div>
  )
}