import Image from "next/image";
import TitleText from "./title_text";

interface TitlesProps {
  title1: string;
  title2?: string;
  subtitle?: string;
  chipText: string;
  chipIcon: string;
  isH1?: boolean;
  className?: string;
}

export default function Titles({ title1, title2, subtitle, chipText, chipIcon, isH1, className }: TitlesProps) {
  return (
    <div className={`flex flex-col gap-10 items-center ${className}`}>
      {/* Chips */}
      <div className="">
        <div className="px-3 py-0.5 bg-[#343333] rounded-full border border-[#3C3A3A] gap-2.5 inline-flex">
          <Image
            src={chipIcon}
            alt="Chip Icon"
            width={20}
            height={20}
            priority
          />
          <div className="text-center text-[#D4C7C7] text-sm font-medium leading-snug">
            {chipText}
          </div>
        </div>
      </div>

      {/* Title */}
      {isH1 ?
        <h1 className="font-semibold text-4xl md:text-7xl text-center mx-6">{content()}</h1> :
        <h2 className="font-semibold text-3xl md:text-5xl text-center mx-4">{content()}</h2>}

      {/* Subtitle */}
      <div className="">
        <div className="text-center leading-normal text-[#D4C7C7] text-lg md:text-xl max-w-2xl mx-4">
          {subtitle}
        </div>
      </div>
    </div>
  )

  function content() {
    return <>
      <TitleText text={title1} className="hidden md:block" />
      {title2 && <TitleText text={title2} className="hidden md:block" />}
      <TitleText text={`${title1}${title2 ? "" + title2 : ""}`} className="md:hidden" />
    </>
  }
}